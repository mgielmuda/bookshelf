import fi.iki.elonen.NanoHTTPD;

import java.io.IOException;
import java.sql.SQLOutput;

public class BookshelfApp extends NanoHTTPD {

    RequestUrlMapper requestUrlMapper = new RequestUrlMapper();

    public BookshelfApp (int port) throws IOException{

        super(port);
        start(5000, false);
        System.out.println("Server has been started.");
    }

    public static void main(String[] args) {
        try{
            new BookshelfApp(8085);
                    }catch (IOException e){
            System.out.println("Server can't started becouse of error: \n" + e);
        }
    }

    @Override
    public Response serve (IHTTPSession session){
        System.out.println();
        return requestUrlMapper.delegateRequest(session);
    }


}
