package com.example.json;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class Driver {
    public static void main(String[] args) {

        ObjectMapper mapper = new ObjectMapper();

//        Student student = mapper.readValue(new File("data/sample.json"), Student.class);
        try {
            Student student = mapper.readValue(new File("data/sample.json"), Student.class);
            Student student2 = mapper.readValue(new File("data/language.json"), Student.class);


            System.out.println(student.getFirstName()+" "+student.getLastName());

            System.out.println(student2.getFirstName()+" "+student2.getLastName());
            System.out.println(Arrays.toString(student2.getLanguage()));
            System.out.println(student2.getAddress().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
